import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:stripe/pages/payments/stripe_home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp
    (
      debugShowCheckedModeBanner: false,
      home: StripHomePage(),
      builder: EasyLoading.init(),
    );
  }
}
